// Create table and control them
function createTable() {
    const selectElement = document.getElementById('size');
    const tableElement = document.getElementById('matrix');
    const stateElement = document.getElementById('state');
    const messageElement = document.getElementById('message');
    let size = selectElement.options[selectElement.selectedIndex].value;
    
    if (size == "0") {
        // Erase table content
        tableElement.innerHTML = '';
        let tr = document.createElement('tr');
        tr.innerHTML = '<td>Selecione um tamanho <b>válido</b> para a matriz.</td>';
        tableElement.appendChild(tr);
    } else {
        resetValues();
        selectElement.value = size;
        // Control calculate button
        if (messageElement.classList.length == 0) {
            messageElement.classList.add('error');
        }
        // Erase table content
        tableElement.innerHTML = '';
        for (i = 0; i < size; i++) {
            let tr = document.createElement('tr');
            let td = '';
            for (j = 0; j < size; j ++) {
                td += '<td><input type="number" step="0.01" id="matrix-'+ i + '-' + j + '"></td>';
            }
            tr.innerHTML = td;
            tableElement.appendChild(tr);
        }

        let tr = document.createElement('tr');
        let td = '';
        for (i = 0; i < size; i++) {
            td += '<td><input type="number" step="0.01" id="matrix-'+ i + '-' + j + '"></td>';
        }
        tr.innerHTML = td;
        stateElement.appendChild(tr);
    }
}

// Calculate values if possible
function calculateValues() {
    const tableElement = document.getElementById('matrix');
    const absDiv = document.getElementById('absorbent-chain');
    const initialDiv = document.getElementById('initial-state');
    const messageElement = document.getElementById('message');
    let rows = tableElement.getElementsByTagName("tr");
    const matrixError = document.getElementById('matrix-message');
    const probError = document.getElementById('prob-message');
    let matrix = [];
    let errorCheck = false;
    let probCheck = false;
    if (rows.length == 0) {
        // Display message
        messageElement.classList.remove('error');
    } else {
        // Remove message if table has rows
        if (messageElement.classList.length == 0) {
            messageElement.classList.add('error');
        }

        for (i = 0; i < rows.length; i++) {
            let line = [];
            let accLine = 0;  
            for (j = 0; j < rows.length; j ++) {
                const value = parseFloat(rows[i].children[j].firstChild.value);
                const el = document.getElementById(rows[i].children[j].firstChild.id);
                if (el.classList.length != 0) {
                    el.classList.remove('border-error');
                }
                // Check if is a number
                if (!isNaN(value)) {
                    line.push(value);
                    accLine += value;
                    // Remove border red from input element
                } else {
                    line.push(value);
                    // Add border red at input element
                    el.classList.add('border-error');
                    errorCheck = true;
                }
            }

            // Check if the sum of all probabilities is 1
            if (accLine != 1) {
                for (k = 0; k < rows.length; k ++) {
                    const el = document.getElementById(rows[i].children[k].firstChild.id); 
                    if (el.classList.length == 0)
                        el.classList.add('border-error');
                }
                probCheck = true;
            }

            matrix.push(line);
        }

        if (probCheck) {
            probError.classList.remove('error');
        } else {
            probError.classList.add('error');
            // Control matrix messages
            if (errorCheck) {
                matrixError.classList.remove('error');
            } else {
                matrixError.classList.add('error');
                
                // Check absorbent state
                if (!hasAbsorbentState(matrix)) {
                    const initialStateElement = document.getElementById('state');
                    const initialStateMsg = document.getElementById('state-message');
                    const initialErrorProbState = document.getElementById('state-prob-message');

                    let initialStates = [];
                    let accLine1 = 0;  
                    
                    // Clear initial state message
                    initialStateMsg.classList.add('error');
                
                    for (i = 0; i < matrix.length; i++) {
                        const value = parseFloat(initialStateElement.children[0].children[i].firstChild.value);
                        initialStates.push(value);
                        accLine1 += value;
                    }

                    if (accLine1 > 1) {
                        initialErrorProbState.classList.remove('error');
                    } else {
                        initialErrorProbState.classList.add('error');
                        if (!hasInitialStateValid(initialStates)) {
                            initialStateMsg.classList.remove('error');
                        } else {
                            if (initialDiv.classList.length != 0){
                                initialDiv.classList.remove('result');
                            }
                            const res =  math.round(math.multiply(initialStates, matrix), 4);
                            printVectorLine('state-prob', res);
                        }
                    }
                } else {
                    // Result div display
                    if (absDiv.classList.length != 0){
                        absDiv.classList.remove('result');
                    }

                    absorbentStates = getAbsorbentStates(matrix);
                    const [matrixN, matrixA] = getMatrixNA(matrix, absorbentStates);
                    const identity = math.identity(rows.length - absorbentStates.length);
                    const n = math.subtract(identity, matrixN);
                    // Steps passing through states before absorption
                    const passProb = math.round(math.inv(n), 4);
                    printMatrix('pass-med', passProb._data, passProb._data.length);
                    // Calculate probability of absorption
                    const absProb = math.multiply(passProb, matrixA);
                    printMatrix('abs-prob', absProb._data, passProb._data.length);
                    // Calculate Steps to absorption
                    const absSteps = math.multiply(passProb, math.ones(passProb._data.length, 1));
                    printMatrix('abs-step', absSteps._data, passProb._data.length);
                }
            }
        }
    }
}

// Check if has at least one absorbent state
function hasAbsorbentState(matrix) {
    let absorbentState = false;
    matrix.forEach(row => {
        if(row.some(elem => elem == 1)) {
            absorbentState = true;
        }
    });
    return absorbentState;
}

function hasInitialStateValid(matrix) {
    return matrix.every(el => !isNaN(el));
}

// Function that gets the indexs of the absorbent states
function getAbsorbentStates(matrix) {
    let absorbentStatesIndex = [];
    for (i = 0; i < matrix.length; i++) {
        for (j = 0; j < matrix.length; j++) {
            if (matrix[i][j] == 1) {
                if (absorbentStatesIndex.indexOf(matrix[i][j]) === -1) {
                    absorbentStatesIndex.push(matrix[i][j]);
                }
            }
        }
    }
    return absorbentStatesIndex;
}

// Function that generates the matrix N with only states not absorbents
// and the matrix A
function getMatrixNA(matrix, absorbentStates) {
    let matrixN = [];
    let matrixA = [];
    for (i = 0; i < matrix.length; i++) {
        let auxiliarMatrixN = [];
        let auxiliarMatrixA = [];
        for (j = 0; j < matrix.length; j++) {
            if (!absorbentStates.some(el => el == i) && !absorbentStates.some(el => el == j)) {
                auxiliarMatrixN.push(matrix[i][j]);
            } else if (absorbentStates.some(el => el == j) && j != i) {
                auxiliarMatrixA.push(matrix[i][j]);
            }
        }
        if (auxiliarMatrixN.length != 0){
            matrixN.push(auxiliarMatrixN);
        }

        if (auxiliarMatrixA.length != 0) {
            matrixA.push(auxiliarMatrixA);
        }
    }
    return [matrixN, matrixA];
}

// Function that write the matrix in a table element with input readonly as tds
function printMatrix(elementId, array, arraySize) {
    const parentEl = document.getElementById(elementId);
    parentEl.innerHTML = '';
    for (i = 0; i < arraySize; i++) {
        const tr = document.createElement('tr');
        let td = '';
        array[i].forEach(el => {
            td += '<td><input value="' + el + '" readonly></td>';
        });
        tr.innerHTML = td;
        parentEl.appendChild(tr);
    }
}

function printVectorLine(elementId, array) {
    const parentEl = document.getElementById(elementId);
    parentEl.innerHTML = '';
   
    const tr = document.createElement('tr');
    let td = '';
    array.forEach(el => {
        td += '<td><input value="' + el + '" readonly></td>';
    });
    tr.innerHTML = td;
    parentEl.appendChild(tr);
}

// Reset all values
function resetValues() {
    const selectElement = document.getElementById('size');
    const tableElement = document.getElementById('matrix');
    const stateElement = document.getElementById('state');
    const absDiv = document.getElementById('absorbent-chain');
    const stateDiv = document.getElementById('initial-state');
    const stateProbsElement = document.getElementById('state-prob');
    const passStepsElement = document.getElementById('pass-med');
    const absStepsElement = document.getElementById('abs-step');
    const absProbsElement = document.getElementById('abs-prob');
    const messageElement = document.getElementById('message');
    const matrixMessageElement = document.getElementById('matrix-message');
    const probMessageElement = document.getElementById('prob-message');
    const stateMessageElement = document.getElementById('state-message');

    selectElement.value = '0';
    tableElement.innerHTML = '';
    stateElement.innerHTML = '';
    stateProbsElement.innerHTML = '';
    passStepsElement.innerHTML = '';
    absStepsElement.innerHTML = '';
    absProbsElement.innerHTML = '';

    if (messageElement !== null && messageElement.classList.length == 0)
        messageElement.classList.add('error');
    if (matrixMessageElement !== null && matrixMessageElement.classList.length == 0)
        matrixMessageElement.classList.add('error');
    if (probMessageElement !== null && probMessageElement.classList.length == 0)
        probMessageElement.classList.add('error');
    if (stateMessageElement !== null && stateMessageElement.classList.length == 0)
        stateMessageElement.classList.add('error');
    if (absDiv !== null && absDiv.classList.length == 0)
        absDiv.classList.add('result');
    if (stateDiv !== null && stateDiv.classList.length == 0)
        stateDiv.classList.add('result');
}

document.getElementById('size').addEventListener('change', createTable);
document.getElementById('button').addEventListener('click', calculateValues);
document.getElementById('reset').addEventListener('click', resetValues);